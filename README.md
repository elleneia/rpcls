# Rock Paper Scissor Lizard Spock game - 
ArjanCodes CODE ROAST of a submitted game for 'roasting'. Well worth the time. He provides the 'before' and 'after' code.  I started with the 'before' code and worked through all of the analysis and refactoring step by step.

## Other code modification possibilities to explore:
- display_rules() has a hard coded rules text. This could possibly be generated from rules.
- It would be nice to see this game in another interface, other than CLI, just to see how
it adapts.
- This code has some duplication, and could possibly be made a little shorter
        elif winner == player_entity:
            self.ui.display_round_winner(self.player_name, player_entity, message)
            self.scoreboard.win_round(self.player_name)
        else:
            self.ui.display_round_winner(self.cpu_name, cpu_entity, message)
            self.scoreboard.win_round(self.cpu_name)
## Links
Refactoring a Rock Paper Scissors Lizard Spock Game //
Code Roast Part 1

https://www.youtube.com/watch?v=Cs9aDesDORc&list=PLC0nd42SBTaNVxWLci4TPoytfzkXxFhCg&index=11

April 8, 2022 ArjanCodes

The code Arjan worked on in this episode is available here:
https://github.com/arjancodes/2022-coderoast-rpsls

Refactoring a Rock Paper Scissors Lizard Spock Game // Part 2

https://www.youtube.com/watch?v=3IsC7cA163