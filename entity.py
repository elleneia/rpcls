from enum import Enum


class Entity(Enum):
    ROCK = "rock"
    PAPER = "paper"
    SCISSOR = "scissor"
    LIZARD = "lizard"
    SPOCK = "spock"

    def __str__(self) -> str:
        return self.value
